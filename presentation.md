# Podman et docker sont sur un bateau...

![logo](https://reseau-loops.github.io/logo.svg)

---

## Images, conteneurs, ...

```mermaid
graph LR
    A[Dockerfile] -->|build| B(Images)
    B -->|tag, rm, ...| B
    B -->|push| D[(Registry)]
    D -->|pull| B
    B -->|run| C[[Containers]]
    C -->|stop, start, rm, ...| C
```

---

## Open Container Initiative

![OCI](https://opencontainers.org/img/logos/OCI-logo.svg)

> The Open Container Initiative is an open governance structure for the express purpose of creating open industry standards around container formats and runtimes.


---

## Open Container Initiative

Docker a beaucoup financé cette initiative.
Grâce à cette initiative, les conteneurs peuvent être exécutés avec d'autres solutions comme podman, Kubernetes, ...

---

## Installation de podman

```bash
sudo apt install podman
# or sudo dnf install podman
# or brew install podman
```

Un peu plus compliqué sur [Windows 10 avec WSL 2.0](https://www.redhat.com/sysadmin/podman-windows-wsl2)
Ou sur un [Mac M1](https://www.cloudassembler.com/post/podman-machine-mac-m1/).

[Source](https://marcusnoble.co.uk/2021-09-01-migrating-from-docker-to-podman/)

---

## Pas de groupe docker pour podman

* Le [groupe docker](https://docs.docker.com/engine/install/linux-postinstall/) accorde des privilèges
  équivalents à ceux de l'utilisateur root
  ([Pour aller plus loin](https://www.grottedubarbu.fr/docker-touche-pas-groupe/))
* podman est _rootless_

---

## rootless ?

```mermaid
graph LR
    A[Docker CLI] -->|docker run| B[(Docker daemon)]
    B -->|runc| C[[Container]]
```

---

## Démo !

[comment]: <> (socket groupe docker)
[comment]: <> (sous docker "apt-get update / install" - appel kernel avec le user root)
[comment]: <> (sous podman "apt-get update / install" - appel kernel avec le user ci-)
[comment]: <> (sous docker 1_gosu - Dockerfile et entry_point - user est ci)
[comment]: <> (sous podman 1_gosu - Podmanfile - root est ci)
[comment]: <> (sous podman "podman pull gosu")

> The core of how [gosu](https://github.com/tianon/gosu#gosu) works is stolen directly from how
  Docker/libcontainer itself starts an application inside a container...

[comment]: <> (sous docker "docker pull wogosu")

---

## podman registries

```bash
cat $HOME/.config/containers/registries.conf

# unqualified-search-registries = ['registry.fedoraproject.org', 'registry.access.redhat.com', 'registry.centos.org', 'docker.io']
```

> List of registries that are contacted in the specified order when pulling a short name image
[Source](https://www.redhat.com/sysadmin/manage-container-registries)

---

## docker-compose ?

- https://github.com/containers/podman-compose
- Création de pods à la volée - [Pour aller plus loin](https://www.tremplin-numerique.org/quest-ce-que-podman-et-en-quoi-differe-t-il-de-docker)

[comment]: <> (podman generate kube)

## Conclusion

- intéressant pour l'admin système
- pods et interface kubernetes
- éviter l'alias !

```bash
alias docker=podman
```
