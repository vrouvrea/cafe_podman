#!/bin/bash

MODE_PRINT="print"

if [ "$MODE" = "$MODE_PRINT" ]; then
  for file in /app/*.md; do
    /usr/local/bin/npx reveal-md ${file} --print ${file%.*}.pdf
  done
else
  echo "Use -e MODE=\"print\" to convert local markdown files to pdf"
  /usr/local/bin/npx reveal-md /app
fi
